import lcm
import numpy as np
import matplotlib.pyplot as plt
import time
import math

import sys
sys.path.append('./lcmtypes')
from ctrlpanel import ctrlPanelData_t

f1 = []
f2 = []
t = []
plt.ion()
fig = plt.figure()
plt.title('Control Panel')


def plot_handler(channel, data):
    msg = ctrlPanelData_t.decode(data)
    f1.append(msg.fader1)
    f2.append(msg.fader2)
    t.append(msg.timestamp)
    fig.canvas.draw()

lc = lcm.LCM()
subscription = lc.subscribe("CTRL_PANEL_DATA", plot_handler)

try:
    while True:
        lc.handle()
        plt.plot(t, f1, 'r')
        plt.plot(t, f2, 'b')

except KeyboardInterrupt:
    with open('f1.dat', 'w') as f:
        for x in f1:
            f.write(str(x) + ",")
    with open('f2.dat', 'w') as f:
        for x in f2:
            f.write(str(x) + ",")
    with open('t.dat', 'w') as f:
        for x in t:
            f.write(str(x) + ",")
    pass

lc.unsubscribe(subscription)
  
