/*********************
*   PID.c
*   simple pid library 
*   pgaskell
**********************/

#include "PID.h"
#include <stdio.h>
#include <stdlib.h>


#define MAX_OUTPUT 1.0
#define MIN_OUTPUT -1.0
#define ITERM_MIN -1.0
#define ITERM_MAX 1.0

PID_t * PID_Init(float Kp, float Ki, float Kd) {
  // allocate memory for PID
  PID_t *pid =  malloc(sizeof(PID_t));

  //initalize values
  pid->pidInput = 0;
  pid->pidOutput = 0;
  pid->pidSetpoint = 0;
  pid->ITerm = 0;
  pid->prevInput = 0;

  // set update rate to 100000 us (0.1s)
  //IMPLEMENT ME!
  
  
  //set output limits, integral limits, tunings
  printf("initializing pid...\r\n");
  //IMPLEMENT ME!

  return pid;
}

void PID_Compute(PID_t* pid) {    
  // Compute PID 
  //IMPLEMENT ME!

  double err = pid->pidSetpoint - pid->pidInput;

  double u_p = pid->kp * err;

  double u_i = pid->ITerm + pid->ki * err;
  //Handle clamping the ITerm
  if (u_i > pid->ITermMax) u_i = pid->ITermMax;
  if (u_i < pid->ITermMin) u_i = pid->ITermMin;
  //Save the ITerm
  pid->ITerm = u_i;

  double vel = pid->pidInput - pid->prevInput;
  double u_d = pid->kd * vel;
  
  //Save the previous input
  pid->prevInput = pid->pidInput;

  pid->pidOutput = u_p + u_i + u_d;

  pid->pidOutput = pid->pidOutput > pid->outputMax ? pid->outputMax : pid->pidOutput;
  pid->pidOutput = pid->pidOutput < pid->outputMin ? pid->outputMin : pid->pidOutput;
}

void PID_SetTunings(PID_t* pid, float Kp, float Ki, float Kd) {
  //scale gains by update rate in seconds for proper units
  float updateRateInSec = ((float) pid->updateRate / 1000000.0);
  //set gains in PID struct
  //IMPLEMENT ME!
  pid->kd = Kd*updateRateInSec;
  pid->ki = Ki*updateRateInSec;
  pid->kp = Kp*updateRateInSec; 
}

void PID_SetOutputLimits(PID_t* pid, float min, float max){
  //set output limits in PID struct
  //IMPLEMENT ME!
  pid->outputMin = min;
  pid->outputMax = max;
}

void PID_SetIntegralLimits(PID_t* pid, float min, float max){
  //set integral limits in PID struct
  //IMPLEMENT ME!
  pid->ITermMin = min;
  pid->ITermMax = max;
}

void PID_SetUpdateRate(PID_t* pid, int updateRate){
  //set integral limits in PID struct
  //IMPLEMENT ME!
  pid->updateRate = updateRate;
}




