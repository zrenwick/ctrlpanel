/**************************************************************
*
*	CtrlPanel Test
*
***************************************************************/

#include "./libBBB/libBBB.h"
#include "math.h"
#include "PID.h"

/* LIBRARIES NEEDED FOR LCM*/
#include <inttypes.h>
#include <lcm/lcm.h>
#include "lcmtypes/ctrlPanelData_t.h"  // ctrlPanel lcm type
#include "lcmtypes/pidData_t.h"  // PID lcm type

//Sensor Limit calibration
//MEASURE FOR YOUR PANEL
#define F1MIN 46
#define F1MAX 1745
#define F2MIN 46
#define F2MAX 1745
#define P1MIN 0
#define P1MAX 1769
#define P2MIN 0
#define P2MAX 1767
#define P3MIN 0
#define P3MAX 1767

//set range of pot control
//YOU NEED TO DEFINE THESE
#define KPMAX 0
#define KIMAX 0
#define KDMAX 0

struct Ctrlpanel_data{
  	int fader1;
  	int fader2;
  	int pots[3];
	int switches[3];
    	int motorSignal;
	float fader1_scaled;
	float fader2_scaled;
	float pots_scaled[3];
};

void InitializePanel(void){

 	/* Initiate Pin 66 Linked to Switch 1 */
    initPin(66);
    setPinDirection(66, INPUT);
	initPin(67);
	setPinDirection(67, INPUT);
	initPin(69);
	setPinDirection(69, INPUT);

        /* Initiate Pin 45 that defines motor direction */
        initPin(45);
        setPinDirection(45, OUTPUT);

        /* Initiate A/D Converter */
        initADC();

        /* Initiate PWM for Servo 1 */
        initPWM(SV1);
        setPWMPeriod(SV1H, SV1, 100000);
        setPWMPolarity(SV1H, SV1, 0);
        setPWMDuty(SV1H, SV1, 0);
        setPWMOnOff(SV1H, SV1, ON);

}

/*******************************************
*       Scaling functions to scale AD readings
*       0 to 1 for pots and -1 to 1 for faders
*******************************************/

float ScaleInt2PosFloat(int input, int min, int max){
    //scale readings to between 0 and 1

    return ((float)input-(float)min)/((float)max - (float)min);
}

float ScaleInt2Float(int input, int min, int max){
    //scale readings to between -1 and 1

    return ScaleInt2PosFloat(input, min, max)*2.0 - 1.0;
}

/*******************************************
*       Read values of the sensors
*       IMPLEMENT THIS FIRST TO TEST
*******************************************/

void ReadPanelValues(struct Ctrlpanel_data * ctrlpanel){

	/* Read ADCs and give time after each reading to settle */
	ctrlpanel->fader1 = readADC(HELPNUM, A0);
	usleep(100);
	ctrlpanel->fader2 = readADC(HELPNUM, A1);
	usleep(100);
	ctrlpanel->pots[0] = readADC(HELPNUM, A2);
	usleep(100);
	ctrlpanel->pots[1] = readADC(HELPNUM, A3);
	usleep(100);
	ctrlpanel->pots[2] = readADC(HELPNUM, A4);
	usleep(100);

	/* Scale the readings */
	ctrlpanel->fader1_scaled = -ScaleInt2Float(ctrlpanel->fader1,F1MIN, F1MAX);
	ctrlpanel->fader2_scaled = -ScaleInt2Float(ctrlpanel->fader2,F1MIN, F1MAX);
	for(int i=0;i<3;i++)
		ctrlpanel->pots_scaled[i] = ScaleInt2PosFloat(ctrlpanel->pots[i],P1MIN, P1MAX);

	/* Read Switches position */
	ctrlpanel->switches[0] = getPinValue(66);
	ctrlpanel->switches[1] = getPinValue(67);
	ctrlpanel->switches[2] = getPinValue(69);

}


void PrintPanelValues(struct Ctrlpanel_data * ctrlpanel){

	// CHANGE TO PRINT ALL INPUTS AND EVENTUAL OUTPUTS YOU ALSO WANT TO CHECK
	printf("fader1: %4d %4f\n",
	ctrlpanel->fader1, ctrlpanel->fader1_scaled);
        printf("fader2: %4d %4f\n",
        ctrlpanel->fader2, ctrlpanel->fader2_scaled);

	for (int i = 0; i<3; ++i)
		printf("pot%d: %4d %4f\n",
        	i+1, ctrlpanel->pots[i], ctrlpanel->pots_scaled[i]);

	printf("motorSignal: %d\n", ctrlpanel->motorSignal);
}

/*******************************************
*
*       CONTROLLERS
*
*******************************************/
void SimpleFaderDriver(struct Ctrlpanel_data * ctrlpanel){

	/* Drive Fader1 motor with Fader 2 position */
	/* Position of Fader 2 is reflected in the speed of Fader 1 */
	/* Fader 2 in middle = Fader 1 does not move */
	/* Fader 2 left = Fader 1 moves to left up to limit */
	/* Fader 2 right = Fader 1 moves to right up to limit */

	ctrlpanel->motorSignal = 200*(ctrlpanel->fader2-1700/2);
      	setPinValue(45, ctrlpanel->motorSignal > 0);
      	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));

}

void BangBang(struct Ctrlpanel_data * ctrlpanel){
	/* Drive the slider in the direction of the error */
	float err = ctrlpanel->fader2_scaled - ctrlpanel->fader1_scaled;
	ctrlpanel->motorSignal = (err < 0?1:-1) * 40000;
	setPinValue(45, err < 0);
	if ( fabs(err) < 0.05f) {
		ctrlpanel->motorSignal = 0;
		setPWMDuty(SV1H, SV1, 0);
	} else {
		setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));
	}
}

void PID(PID_t *pid, struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
  pid->pidInput = ctrlpanel->fader1_scaled;
  pid->pidSetpoint = ctrlpanel->fader2_scaled;

  //Set the gains (get swole)
  float kp = !ctrlpanel->switches[0] ? (ctrlpanel->pots_scaled[0]*-70000000.0)-25000000 : 0.0;
  float ki = !ctrlpanel->switches[1] ? (ctrlpanel->pots_scaled[1]*-600000.0)-2000000 : 0.0;
  float kd = !ctrlpanel->switches[2] ? (ctrlpanel->pots_scaled[2]*70000000.0)+3500000 : 0.0;

  PID_SetTunings(pid, kp, ki, kd);

  PID_Compute(pid);
  ctrlpanel->motorSignal = pid->pidOutput;
  setPinValue(45, ctrlpanel->motorSignal > 0);
  setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));
}


/*******************************************
*
*       LCM Message Preparation
*
*******************************************/

void CtrlPanelDataLCM(struct Ctrlpanel_data * ctrlpanel, const char *channel, lcm_t * lcm){

        ctrlPanelData_t msg = {};

	    /* Prepare message to send over lcm channel*/
        msg.timestamp = utime_now();
        msg.fader1 = ctrlpanel->fader1;
        msg.fader2 = ctrlpanel->fader2;
        msg.pots[0] = ctrlpanel->pots[0];
        msg.pots[1] = ctrlpanel->pots[1];
        msg.pots[2] = ctrlpanel->pots[2];
        msg.switches[0] = ctrlpanel->switches[0];
        msg.switches[1] = ctrlpanel->switches[1];
        msg.switches[2] = ctrlpanel->switches[2];

        ctrlPanelData_t_publish(lcm, channel, &msg);
}

void PIDLCM(struct Ctrlpanel_data * ctrlpanel, PID_t *pid, const char *channel, lcm_t * lcm){

	pidData_t msg = {};

    /* Prepare message to send over lcm channel*/
    //IMPLEMENT ME!
	msg.timestamp = utime_now();
	msg.pidInput = pid->pidInput;
	msg.pidSetpoint = pid->pidSetpoint;
	msg.pidOutput = pid->pidOutput;
	msg.Kp = pid->kp;
	msg.Ki = pid->ki;
	msg.Kd = pid->kd;

	pidData_t_publish(lcm, channel, &msg);
}



/*******************************************
*
*       MAIN FUNCTION
*
*******************************************/
int main()
{

	struct Ctrlpanel_data ctrlpanel;

	// For Controllers
	int64_t thisTime, lastTime, timeDiff;
	int updateRate = 5000;

	// PID
	PID_t *pid;
	pid = PID_Init(0,0,0);

	PID_SetUpdateRate(pid, updateRate);
	PID_SetOutputLimits(pid, -60000, 60000);
	PID_SetIntegralLimits(pid, -40000, 40000);

        // LCM
        const char *channel1 = "CTRL_PANEL_DATA";   // LCM channel to publish messages
        const char *channel2 = "PIDDATA";   // LCM channel to publish messages
        lcm_t * lcm = lcm_create(NULL);
        if(!lcm) {
                return 1;
                }

	// Panel Initialization
	InitializePanel();

	/* Never ending loop */
	while(1)
	{
		ReadPanelValues(&ctrlpanel);

		// Control at a specified frequency
		thisTime = utime_now();
                timeDiff = thisTime - lastTime;
                if(timeDiff >= updateRate){
			lastTime = thisTime;
			//SimpleFaderDriver(&ctrlpanel);
			//BangBang(&ctrlpanel);
			PID(pid,&ctrlpanel);
		}

		PrintPanelValues(&ctrlpanel);

		// UNCOMMENT TO INCLUDE LCM MESSAGE
		CtrlPanelDataLCM(&ctrlpanel,channel1,lcm);
		PIDLCM(&ctrlpanel,pid,channel2,lcm);

	}

	return 0;
}
