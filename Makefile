#
DEPS = ./libBBB/libBBB.c PID.c
SOURCES = ctrlPanel.c  
EXECUTABLE = ctrlpanel
CC=gcc
CFLAGS = -g -Wall -std=gnu99 `pkg-config --cflags lcm` -lm
LDFLAGS = `pkg-config --libs lcm`

.PHONY: all clean lcmtypes

all: lcmtypes $(EXECUTABLE)

clean:
	@$(MAKE) -C lcmtypes clean
	rm -f *.o *~ $(EXECUTABLE)

lcmtypes:
	@$(MAKE) -C lcmtypes

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

lcmtypes/%.o: lcmtypes/%.c
	$(CC) $(CFLAGS) -c $^ -o $@ $(LDFLAGS)


ctrlpanel: ctrlPanel.c lcmtypes/ctrlPanelData_t.o lcmtypes/pidData_t.o $(DEPS)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)
